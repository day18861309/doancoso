﻿using _2180601738_PhamThanhTu_WebBanXeHoi.Models;
using DoAnCoSo_Website_DDHTTHPT.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Text.RegularExpressions;

namespace DoAnCoSo_Website_DDHTTHPT.DataAccess
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        public DbSet<CategorySubject> categorySubjects { get; set; }
        public DbSet<Comment> comments { get; set; }
        public DbSet<Team> teams { get; set; }
        public DbSet<Message> messages { get; set; }
        public DbSet<Post> posts { get; set; }
        public DbSet<Student> students { get; set; }
        public DbSet<Subject> subjects { get; set; }
        public DbSet<Teacher> teachers { get; set; }
        public DbSet<TeacherImage> teacherImages { get; set; }
        public DbSet<StudentImage> studentImages { get; set; }

    }
}
