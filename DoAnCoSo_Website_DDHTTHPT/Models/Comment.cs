﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class Comment
    {
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        public int UserId { get; set; }

        public int PostId { get; set; }

        public DateTime CommentedTime { get; set; }
    }
}
