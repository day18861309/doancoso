﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class Post
    {
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        public int UserId { get; set; }

        public DateTime PostedTime { get; set; }

        public int Likes { get; set; }

        public int Comments { get; set; }

        // Các thuộc tính khác liên quan
    }
}
