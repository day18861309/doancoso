﻿using DoAnCoSo_Website_DDHTTHPT.Models;

namespace _2180601738_PhamThanhTu_WebBanXeHoi.Models
{
    public class TeacherImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int teacherId { get; set; }
        public Teacher? teacher { get; set; }
    }
}
