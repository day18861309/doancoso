﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class Message
    {
        public int Id { get; set; }

        [Required]
        public string Content { get; set; }

        public int SenderId { get; set; }

        public int ReceiverId { get; set; }

        public DateTime SentTime { get; set; }
    }

}
