﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class Team
    {
        public int Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        // Các thành viên và quyền truy cập
    }
}
