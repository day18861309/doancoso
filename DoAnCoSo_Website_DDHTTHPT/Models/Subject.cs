﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class Subject
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Vui lòng nhập tên môn học.")]
        public string Name { get; set; }

        public string Description { get; set; }

        public string Teacher { get; set; }

    }
}
