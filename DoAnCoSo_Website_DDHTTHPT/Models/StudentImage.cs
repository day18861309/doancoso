﻿using DoAnCoSo_Website_DDHTTHPT.Models;

namespace _2180601738_PhamThanhTu_WebBanXeHoi.Models
{
    public class StudentImage
    {
        public int Id { get; set; }
        public string Url { get; set; }
        public int studentId { get; set; }
        public Student? student { get; set; }
    }
}
