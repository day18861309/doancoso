﻿using Microsoft.AspNetCore.Identity;
using System.Numerics;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class ApplicationUser : IdentityUser
    {
        public string FullName { get; set; }
        public List<CategorySubject>? categorySubjects { get; set; }
        public List<Comment>? comments { get; set; }
        public List<Message>? messages { get; set; }
        public List<Post>? posts { get; set; }
        public List<Subject>? subjects { get; set; }
        public List<Teacher>? teachers { get; set; }
    }
}
