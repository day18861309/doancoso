﻿using System.ComponentModel.DataAnnotations;

namespace DoAnCoSo_Website_DDHTTHPT.Models
{
    public class CategorySubject
    {
        public int Id { get; set; }
        [Required, StringLength(50)]
        public string Name { get; set; }
        public List<Subject>? Subjects { get; set; }
    }

}
