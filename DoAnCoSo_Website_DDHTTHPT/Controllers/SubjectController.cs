﻿using DoAnCoSo_Website_DDHTTHPT.Repository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace DoAnCoSo_Website_DDHTTHPT.Controllers
{
    public class SubjectController : Controller
    {
        private readonly ISubjectRepository _subjectRepository;

        public SubjectController(ISubjectRepository subjectRepository)
        {
            _subjectRepository = subjectRepository;
        }

        public async Task<IActionResult> Index()
        {
            var subject = await _subjectRepository.GetAllAsync();
            return View(subject);
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
