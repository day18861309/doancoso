﻿using DoAnCoSo_Website_DDHTTHPT.DataAccess;
using DoAnCoSo_Website_DDHTTHPT.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public class EFCommentRepository : ICommentRepository
    {
        private readonly ApplicationDbContext _context;
        public EFCommentRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Comment>> GetAllAsync()
        {
            return await _context.comments.ToListAsync();
        }

        public async Task<Comment> GetByIdAsync(int id)
        {
            return await _context.comments.FindAsync(id);
        }

        public async Task AddAsync(Comment comment)
        {
            _context.comments.Add(comment);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Comment comment)
        {
            _context.comments.Update(comment);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var comment = await _context.comments.FindAsync(id);
            _context.comments.Remove(comment);
            await _context.SaveChangesAsync();
        }
    }
}
