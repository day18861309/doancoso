﻿using DoAnCoSo_Website_DDHTTHPT.Models;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public interface ICommentRepository
    {
        Task<IEnumerable<Comment>> GetAllAsync();
        Task<Comment> GetByIdAsync(int id);
        Task AddAsync(Comment comment);
        Task UpdateAsync(Comment comment);
        Task DeleteAsync(int id);
    }
}
