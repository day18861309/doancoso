﻿using DoAnCoSo_Website_DDHTTHPT.DataAccess;
using DoAnCoSo_Website_DDHTTHPT.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public class EFPostRepository : IPostRepository
    {
        private readonly ApplicationDbContext _context;
        public EFPostRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Post>> GetAllAsync()
        {
            return await _context.posts.ToListAsync();
        }

        public async Task<Post> GetByIdAsync(int id)
        {
            return await _context.posts.FindAsync(id);
        }

        public async Task AddAsync(Post post)
        {
            _context.posts.Add(post);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Post post)
        {
            _context.posts.Update(post);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var post = await _context.posts.FindAsync(id);
            _context.posts.Remove(post);
            await _context.SaveChangesAsync();
        }
    }
}
