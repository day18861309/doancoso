﻿using DoAnCoSo_Website_DDHTTHPT.Models;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public interface IPostRepository
    {
        Task<IEnumerable<Post>> GetAllAsync();
        Task<Post> GetByIdAsync(int id);
        Task AddAsync(Post post);
        Task UpdateAsync(Post post);
        Task DeleteAsync(int id);
    }
}
