﻿using DoAnCoSo_Website_DDHTTHPT.Models;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public interface ITeacherRepository
    {
        Task<IEnumerable<Teacher>> GetAllAsync();
        Task<Teacher> GetByIdAsync(int id);
        Task AddAsync(Teacher teacher);
        Task UpdateAsync(Teacher teacher);
        Task DeleteAsync(int id);
    }
}
