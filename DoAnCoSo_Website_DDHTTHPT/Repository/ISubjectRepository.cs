﻿using DoAnCoSo_Website_DDHTTHPT.Models;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public interface ISubjectRepository
    {
        Task<IEnumerable<Subject>> GetAllAsync();
        Task<Subject> GetByIdAsync(int id);
        Task AddAsync(Subject student);
        Task UpdateAsync(Subject student);
        Task DeleteAsync(int id);
    }
}
