﻿using DoAnCoSo_Website_DDHTTHPT.DataAccess;
using DoAnCoSo_Website_DDHTTHPT.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public class EFCategorySubjectRepository : ICategorySubjectRepository
    {

        private readonly ApplicationDbContext _context;
        public EFCategorySubjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<CategorySubject>> GetAllAsync()
        {
            return await _context.categorySubjects.ToListAsync();
        }

        public async Task<CategorySubject> GetByIdAsync(int id)
        {
            return await _context.categorySubjects.FindAsync(id);
        }

        public async Task AddAsync(CategorySubject categorySubject)
        {
            _context.categorySubjects.Add(categorySubject);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(CategorySubject categorySubject)
        {
            _context.categorySubjects.Update(categorySubject);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var categorySubject = await _context.categorySubjects.FindAsync(id);
            _context.categorySubjects.Remove(categorySubject);
            await _context.SaveChangesAsync();
        }

    }
}
