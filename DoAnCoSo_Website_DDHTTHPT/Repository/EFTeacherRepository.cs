﻿using DoAnCoSo_Website_DDHTTHPT.DataAccess;
using DoAnCoSo_Website_DDHTTHPT.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public class EFTeacherRepository : ITeacherRepository
    {
        private readonly ApplicationDbContext _context;
        public EFTeacherRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Teacher>> GetAllAsync()
        {
            return await _context.teachers.ToListAsync();
        }

        public async Task<Teacher> GetByIdAsync(int id)
        {
            return await _context.teachers.FindAsync(id);
        }

        public async Task AddAsync(Teacher teacher)
        {
            _context.teachers.Add(teacher);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Teacher teacher)
        {
            _context.teachers.Update(teacher);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var teacher = await _context.teachers.FindAsync(id);
            _context.teachers.Remove(teacher);
            await _context.SaveChangesAsync();
        }
    }
}
