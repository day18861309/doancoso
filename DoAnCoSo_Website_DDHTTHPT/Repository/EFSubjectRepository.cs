﻿using DoAnCoSo_Website_DDHTTHPT.DataAccess;
using DoAnCoSo_Website_DDHTTHPT.Models;
using Microsoft.EntityFrameworkCore;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public class EFSubjectRepository : ISubjectRepository
    {
        private readonly ApplicationDbContext _context;
        public EFSubjectRepository(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IEnumerable<Subject>> GetAllAsync()
        {
            return await _context.subjects.ToListAsync();
        }

        public async Task<Subject> GetByIdAsync(int id)
        {
            return await _context.subjects.FindAsync(id);
        }

        public async Task AddAsync(Subject subject)
        {
            _context.subjects.Add(subject);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(Subject subject)
        {
            _context.subjects.Update(subject);
            await _context.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var subject = await _context.subjects.FindAsync(id);
            _context.subjects.Remove(subject);
            await _context.SaveChangesAsync();
        }
        public List<Subject> GetAll()
        {
            return _context.subjects.ToList();
        }
    }
}
