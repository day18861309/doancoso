﻿using DoAnCoSo_Website_DDHTTHPT.Models;
using System.Numerics;

namespace DoAnCoSo_Website_DDHTTHPT.Repository
{
    public interface ICategorySubjectRepository
    {
        Task<IEnumerable<CategorySubject>> GetAllAsync();   
        Task<CategorySubject> GetByIdAsync(int id);
        Task AddAsync(CategorySubject categorySubject);
        Task UpdateAsync(CategorySubject doctor);
        Task DeleteAsync(int id);
        
    }
}
